<?php

/**
 * @file
 * SvgPath2Poly class for calculating points along a SVG path.
 */

class SvgPath2Poly {

  private $poly = array();
  private $start_point = array();
  private $curr_point = array();
  private $control_point = array();
  private $previous_command = '';
  private static $cache = array();
  private $curve_points = NULL;
  private $translate = array();

  public function __construct($curve_points = 20, $translate = array(0, 0)) {
    $this->curve_points = $curve_points;
    $this->translate = $translate;
  }

  public function command($command) {
    $method = "command_" . $command['command'];
    if (ereg("[a-z]", $command['command'])) {
      $method .= "_rel";
    }
    else {
      $method .= "_abs";
    }
    $this->{$method}($command['parameters']);
  }

  public function command_m_abs($params) {
    $this->poly[] = $this->curr_point = $this->start_point = array_splice($params , 0 , 2);
    while (count($params)>1) {
      $this->poly[] = $this->curr_point = array_splice($params , 0 , 2);
    }
    $this->control_point = $this->curr_point;
    $this->previous_command = 'M';
  }

  public function command_m_rel($params) {
    $this->command_m_abs(array_splice($params , 0 , 2));
    while (count($params)>1) {
      $this->poly[] = $this->curr_point = $this->array_add(array_splice($params , 0 , 2), $this->curr_point);
    }
    $this->control_point = $this->curr_point;
    $this->previous_command = 'm';
  }

  public function command_l_abs($params) {
    while (count($params)>1) {
      $this->poly[] = $this->curr_point = array_splice($params , 0 , 2);
    }
    $this->control_point = $this->curr_point;
    $this->previous_command = 'L';
  }

  public function command_l_rel($params) {
    while (count($params)>1) {
      $this->poly[] = $this->curr_point = $this->array_add(array_splice($params , 0 , 2), $this->curr_point);
    }
    $this->control_point = $this->curr_point;
    $this->previous_command = 'l';
  }

  public function command_h_abs($params) {
    $params = array(array_pop($params), $this->curr_point[1]);
    $this->L($params);
    $this->previous_command = 'H';
  }

  public function command_h_rel($params) {
    $params = array(array_pop($params), 0);
    $this->l_rel($params);
    $this->previous_command = 'h';
  }

  public function command_v_abs($params) {
    $params = array($this->curr_point[0], array_pop($params));
    $this->L($params);
    $this->previous_command = 'V';
  }

  public function command_v_rel($params) {
    $params = array(0, array_pop($params));
    $this->l_rel($params);
    $this->previous_command = 'v';
  }

  public function command_c_abs($params) {
    while (count($params)>5) {
      $points = array();
      $points[] = $this->curr_point;
      for ($i = 0; $i < 3; $i++) {
        $points[] = array_splice($params , 0 , 2);
      }
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
    }
    $this->control_point = $this->array_reflect($points[2], $this->curr_point);
    $this->previous_command = 'C';
  }

  public function command_c_rel($params) {
    while (count($params)>5) {
      $points = array();
      $points[] = $this->curr_point;
      for ($i = 0; $i < 3; $i++) {
        $points[] = $this->array_add(array_splice($params , 0 , 2), $this->curr_point);
      }
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
      $this->control_point = $this->array_reflect($points[1], $this->curr_point);
    }
    $this->previous_command = 'c';
  }

  public function command_s_abs($params) {
    while (count($params)>3) {
      $points = array();
      $points[] = $this->curr_point;
      $points[] = in_array($this->previous_command, array('C', 'c', 'S', 's')) ? $this->control_point : $this->curr_point;
      for ($i = 0; $i < 2; $i++) {
        $points[] = array_splice($params , 0 , 2);
      }
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
    }
    $this->control_point = $this->array_reflect($points[1], $this->curr_point);
    $this->previous_command = 'C';
  }

  public function command_s_rel($params) {
    while (count($params)>3) {
      $points = array();
      $points[] = $this->curr_point;
      $points[] = in_array($this->previous_command, array('C', 'c', 'S', 's')) ? $this->control_point : $this->curr_point;
      for ($i = 0; $i < 2; $i++) {
        $points[] = $this->array_add(array_splice($params , 0 , 2), $this->curr_point);
      }
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
      $this->control_point = $this->array_reflect($points[1], $this->curr_point);
    }
    $this->previous_command = 's';
  }

  public function command_q_abs($params) {
    while (count($params)>3) {
      $points = array();
      $points[] = $this->curr_point;
      for ($i = 0; $i < 2; $i++) {
        $points[] = array_splice($params , 0 , 2);
      }
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
    }
    $this->control_point = $this->array_reflect($points[1], $this->curr_point);
    $this->previous_command = 'Q';
  }

  public function command_q_rel($params) {
    while (count($params)>3) {
      $points = array();
      $points[] = $this->curr_point;
      for ($i = 0; $i < 2; $i++) {
        $points[] = $this->array_add(array_splice($params , 0 , 2), $this->curr_point);
      }
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
      $this->control_point = $this->array_reflect($points[1], $this->curr_point);
    }
    $this->previous_command = 'q';
  }

  public function command_t_abs($params) {
    while (count($params)) {
      $points = array();
      $points[] = $this->curr_point;
      $points[] = in_array($this->previous_command, array('Q', 'q', 'T', 't')) ? $this->control_point : $this->curr_point;
      $points[] = array_splice($params , 0 , 2);
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
    }
    $this->control_point = $this->array_reflect($points[1], $this->curr_point);
    $this->previous_command = 'T';
  }

  public function command_t_rel($params) {
    while (count($params)) {
      $points = array();
      $points[] = $this->curr_point;
      $points[] = in_array($this->previous_command, array('Q', 'q', 'T', 't')) ? $this->control_point : $this->curr_point;
      $points[] = $this->array_add(array_splice($params , 0 , 2), $this->curr_point);
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $t = $i / $this->curve_points;
        $this->poly[] = $this->curr_point = $this->bezier($points, $t);
      }
      $this->control_point = $this->array_reflect($points[1], $this->curr_point);
    }
    $this->previous_command = 't';
  }

  public function command_a_abs($params) {
    while (count($params) > 6) {
      list($rx, $ry, $fi, $fa, $fs, $x2, $y2) = array_splice($params , 0 , 7);
      $x1 = $this->curr_point[0];
      $y1 = $this->curr_point[1];
      if ($rx == 0 || $ry == 0) {
        $this->command_l_abs(array($x2, $y2));
        $this->previous_command = 'A';
        return;
      }
      $rx = abs($rx);
      $ry = abs($ry);
      $fi_rad = deg2rad($fi);
      $x1p = cos($fi_rad) * ($x1 - $x2) / 2 + sin($fi_rad) * ($y1 -$y2) / 2;
      $y1p = -sin($fi_rad) * ($x1 - $x2) / 2 + cos($fi_rad) * ($y1 -$y2) / 2;
      $delta = pow($x1p, 2) / pow($rx, 2) + pow($y1p, 2) / pow($ry, 2);
      if ($delta > 1) {
        $rx = sqrt($delta) * $rx;
        $ry = sqrt($delta) * $ry;
      }
      $cp_sign = $fa == $fs ? -1 : 1;
      $cxp = $cp_sign * sqrt((pow($rx, 2) * pow($ry, 2) - pow($rx, 2) * pow($y1p, 2) - pow($ry, 2) * pow($x1p, 2))/(pow($rx, 2) * pow($y1p, 2) + pow($ry, 2) * pow($x1p, 2))) * $rx * $y1p / $ry;
      $cyp = $cp_sign * sqrt((pow($rx, 2) * pow($ry, 2) - pow($rx, 2) * pow($y1p, 2) - pow($ry, 2) * pow($x1p, 2))/(pow($rx, 2) * pow($y1p, 2) + pow($ry, 2) * pow($x1p, 2))) * (-1) * $yx * $x1p / $rx;
      $cx = cos($fi_rad) * $cxp - sin($fi_rad) * $cyp + ($x1 + $x2) / 2;
      $cy = sin($fi_rad) * $cxp + cos($fi_rad) * $cyp + ($y1 + $y2) / 2;
      $theta_1 = $this->vector_angle(1, 0, ($x1p - $cxp) / $rx, ($y1p - $cyp) / $ry);
      $delta_theta = $this->vector_angle(($x1p - $cxp) / $rx, ($y1p - $cyp) / $ry, (- $x1p - $cxp) / $rx, (- $y1p - $cyp) / $ry );
      if ($fs == 0) {
        if ($delta_theta > 0) {
          $delta -= 2 * pi();
        }
      }
      else {
        if ($delta_theta < 0) {
          $delta += 2 * pi();
        }
      }
      for ($i = 1; $i <= $this->curve_points; $i++) {
        $theta = $theta_1 + $i / $this->curve_points * $delta_theta;
        $this->poly[] = $this->curr_point = $this->eliptic_arc($cx, $cy, $rx, $ry, $fi, $theta);
      }
    }
    $this->control_point = $this->curr_point;
    $this->previous_command = 'A';
  }

  public function command_a_rel($params) {
    while (count($params) > 6) {
      $sub_params = array_splice($params , 0 , 5);
      $endpoint = $this->array_add(array_splice($params , 0 , 2), $this->curr_point);
      $sub_params = array_merge($sub_params, $endpoint);
      $this->command_a_abs($sub_params);
    }
    $this->previous_command = 'a';
  }

  public function command_z_abs($params) {
    $this->poly[] = $this->start_point;
    $this->previous_command = 'Z';
  }

  public function command_z_rel($params) {
    $this->command_z_abs($params);
    $this->previous_command = 'z';
  }

  public function get_poly() {
    return $this->apply_translate()->poly;
  }

  private function apply_translate() {
    foreach ($this->poly as $key => $point) {
      $this->poly[$key] = $this->array_add($point, $this->translate);
    }
    return $this;
  }

  private function array_add($a1, $a2) {
    $aRes = $a1;
    foreach (array_slice(func_get_args(), 1) as $aRay) {
      foreach (array_intersect_key($aRay, $aRes) as $key => $val) {
        $aRes[$key] += $val;
      }
      $aRes += $aRay;
    }
    return $aRes;
  }

  private function array_reflect($a1, $a2) {
    $aRes = array();
    foreach (array_intersect_key($a1, $a2) as $key => $val) {
      $aRes[$key] = 2 * $a2[$key] - $val ;
    }
    return $aRes;
  }

  private function bezier($points, $t) {
    if ($t<0 || $t>1) {
      return FALSE;
    }
    $n = count($points) -1;
    $result = array(0, 0);
    for ($i=0; $i<= $n; $i++) {
      $result[0] += $this->binomial_coefficient($i, $n) * pow(1-$t, $n-$i) * pow($t, $i) * $points[$i][0];
      $result[1] += $this->binomial_coefficient($i, $n) * pow(1-$t, $n-$i) * pow($t, $i) * $points[$i][1];
    }
    return $result;
  }

  private function binomial_coefficient($k, $n) {
      $j = $res = 1;
      if ($k < 0 || $k > $n) {
        return 0;
      }
      if (($n - $k) < $k) {
        $k = $n - $k;
      }
      while ($j <= $k) {
        $res *= $n--;
        $res /= $j++;
      }

      return $res;
  }

  private function vector_angle($x1, $y1, $x2, $y2) {
    $sign = $x1*$y2 - $y1*$x2;
    $sign = $sign / abs($sign);
    $dot_product = $x1*$x2 + $y1*$y2;
    $length1 = sqrt(pow($x1, 2) + pow($y1, 2));
    $length2 = sqrt(pow($x2, 2) + pow($y2, 2));
    return $sign * acos($dot_product / ($length1 * $length2));
  }

  private function eliptic_arc($cx, $cy, $rx, $ry, $fi, $theta) {
    $point = array();
    $point[] = cos($fi) * $rx * cos($theta) - sin($fi) * $ry * sin($theta) + $cx;
    $point[] = sin($fi) * $rx * cos($theta) + cos($fi) * $ry * sin($theta) + $cy;
    return $point;
  }

}
