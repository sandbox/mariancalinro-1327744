<?php

/**
 * @file
 * AreaPath class for generating <area ..> markup from an array of points.
 */

class AreaPath {

  public $type = "area";
  public $group = array();
  public $points = array();
  public $x_scale = 1;
  public $y_scale = 1;
  public $id = NULL;
  public $class = NULL;
  public $result = '';

  public function generate_markup() {
    if ($this->type == "group") {
      foreach ($this->group as $sub_group) {
        $sub_group->x_scale = $this->x_scale;
        $sub_group->y_scale = $this->y_scale;
        if (!empty($this->id)) {
          $sub_group->class = $this->id;
        }
        $this->result .= $sub_group->generate_markup();
      }
    }
    else {
      foreach ($this->points as $key => $value) {
        $value[0] = $value[0] * $this->x_scale;
        $value[1] = $value[1] * $this->y_scale;
        $this->points[$key] = array_map('intval', array_map('round', $value));
      }
      $this->result .= '<area href="#" shape="poly"';
      if (!empty($this->id)) {
        $this->result .= ' id="' . $this->id . '" alt="' . $this->id . '" title="' . $this->id . '"';
      }
      if (!empty($this->class)) {
        $this->result .= ' class="' . $this->class . '"';
        if (empty($this->id)) {
          $this->result .= ' alt="' . $this->class . '" title="' . $this->class . '"';
        }
      }
      $this->result .= ' coords="' . $this->r_implode(',', $this->points) . '" />' . "\n";
    }
    return $this->result;
  }

  private function r_implode($glue, $pieces) {
    foreach ($pieces as $r_pieces) {
      if (is_array($r_pieces)) {
        $retVal[] = $this->r_implode($glue, $r_pieces);
      }
      else {
        $retVal[] = $r_pieces;
      }
    }
    return implode($glue, $retVal);
  }
}